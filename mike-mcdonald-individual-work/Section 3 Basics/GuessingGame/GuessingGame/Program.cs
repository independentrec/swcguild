﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame
{
    class Program
    {
        static void Main(string[] args)
        {
            int theAnswer;
            int playerGuess;
            string playerInput;
            bool isNumberGuessed = false;

            Random r = new Random();
            theAnswer = r.Next(1, 21);

            do
            {
                // get player input
                Console.WriteLine("Enter your guess (1-20): ");
                playerInput = Console.ReadLine();

                //attempt to convert the string to a number
                if (int.TryParse(playerInput, out playerGuess))
                {
                    if (playerGuess == theAnswer)
                    {
                        Console.WriteLine("You Win!!!");
                        isNumberGuessed = true;
                    }
                    else
                    {
                        Console.WriteLine("Price is Wrong Bitch!");
                        if (playerGuess > theAnswer)
                        {
                            Console.WriteLine("Too High!");
                        }
                        else
                        {
                            Console.WriteLine("Too Low!");
                        }
                    }
                }
                else
                {
                    Console.WriteLine("That wasn't a valid number noob!");
                }

            } while (!isNumberGuessed);

            Console.WriteLine("Press any key to quit.");
            Console.ReadKey();
        }
    }
}
/*
Spec:
 
1) Before starting the game, ask for the player’s name.Then use their name in all of the
messages and notifications throughout the game.

    Console.WriteLine
    define field variable so it can go across program?
    call variable.

2) If the player doesn’t enter a number within the valid range (i.e. 1-20), give them a
message that indicates the error and ask them to try again.

    tryparse?

3) Allow the player to quit anytime throughout game play by entering Q.

    What does quit game mean? exit cmd? end program? investigate.

4) Keep track of how many attempts it takes for the player to guess the right number and
let them know when the game is over.

    investigate - times player guess taken (while loop? unknown tries.)

5) If the player guesses the correct answer on the first try, give them a special victory
message.

    investigate - times player guess taken (if loop? known tries.)

6) Change the font colors in the game for different messages:
a)All standard instructions in the game should be presented in white font
b) All error messages should be presented in red font
c) All victory messages should be presented in green font

    http://www.dotnetperls.com/console-color

7) Allow the user to select different modes at the beginning of the game.Each mode
would change the range of numbers that can be used to choose from:
a)Easy mode 1-5
b)Normal mode 1-20
c)Hard mode 1-50

    investigate defining range of guess.

    */
