﻿using System;

namespace Warmups.BLL
{
    public class Strings
    {

        public string SayHi(string name)
        {
            return String.Format("Hello {0}!", name);
        }

        public string Abba(string a, string b)
        {
            return String.Format(a+b+b+a);
        }

        public string MakeTags(string tag, string content)
        {
            return String.Format("<{0}>{1}</{0}>", tag, content);
        }

        public string InsertWord(string container, string word)
        {
            return String.Format(container.Insert(2, word));
        
        }

        public string MultipleEndings(string str)
        {
            return String.Format(str.Substring(str.Length-2) + str.Substring(str.Length-2) + str.Substring(str.Length-2));
        }

        public string FirstHalf(string str)
        {
            int x = str.Length;

        }

        public string TrimOne(string str)
        {
            return String.Format(str.Remove(0)); 
        }

        public string LongInMiddle(string a, string b)
        {
            throw new NotImplementedException();
        }

        public string RotateLeft2(string str)
        {
            throw new NotImplementedException();
        }

        public string RotateRight2(string str)
        {
            throw new NotImplementedException();
        }

        public string TakeOne(string str, bool fromFront)
        {
            throw new NotImplementedException();
        }

        public string MiddleTwo(string str)
        {
            throw new NotImplementedException();
        }

        public bool EndsWithLy(string str)
        {
            throw new NotImplementedException();
        }

        public string FrontAndBack(string str, int n)
        {
            throw new NotImplementedException();
        }

        public string TakeTwoFromPosition(string str, int n)
        {
            throw new NotImplementedException();
        }

        public bool HasBad(string str)
        {
            throw new NotImplementedException();
        }

        public string AtFirst(string str)
        {
            throw new NotImplementedException();
        }

        public string LastChars(string a, string b)
        {
            throw new NotImplementedException();
        }

        public string ConCat(string a, string b)
        {
            throw new NotImplementedException();
        }

        public string SwapLast(string str)
        {
            throw new NotImplementedException();
        }

        public bool FrontAgain(string str)
        {
            throw new NotImplementedException();
        }

        public string MinCat(string a, string b)
        {
            throw new NotImplementedException();
        }

        public string TweakFront(string str)
        {
            throw new NotImplementedException();
        }

        public string StripX(string str)
        {
            throw new NotImplementedException();
        }
    }
}
