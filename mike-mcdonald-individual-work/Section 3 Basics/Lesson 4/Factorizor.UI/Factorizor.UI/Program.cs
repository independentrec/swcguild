﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factorizor.UI
{
    class Calculator
    {
        static void Main(string[] args)
        {
            
            while(true)
            {
                Console.Write("What number would you like to factor?");
                string input = Console.ReadLine();

                int number;

                if (int.TryParse(input, out number))
                {
                    Console.WriteLine(number);
                    break;
                }
                else
                {
                    Console.WriteLine("That is not a number. Please enter a number to factor.");
                }
                /*
                    notes from solution.

            A. Calculator Class - defined at top - verify this is ok when running - else create new program.
                1) Solve math how to find all factors in number;

                    HINT: precidence video - 
                    
                    - if you do a modulus and the result is zero then the number is a factor. ex: 15 % 5.
                    - 4 % 2 = 0 EVEN; 5 % 2 = 1 ODD
                    - an easy way to find a prime number is to loop from 2 to one less than the number
                    doing a modulus each time. if none of the modulus calculations reach 0 then the number is prime.
                   
                    i) If number is even then while loop divide by 2 until result is 1
                    (How to list factors out? console write line result of x/2 - investigate)
                    
                    ii) Else number is odd divide by 3 until number is not divisable by 3; then use
                    next prime, 7, 11, 13, 17, 19, 23 until result is 1 - investigate easiest solution and
                    how to list factors out. 
                       
                    iii) Console.WriteLine "The factors of x are:"

                2) use result from A, 1, iii - if adding all factors numbers = x then number is perfect else not perfect. 
                    could also list first 6 perfect numbers and if equal nx is perfect if not X is not. perfect 7 is out of 
                    range.
                    
                   Console.WriteLine "x is a perfect number" else "x is not a perfect number"

                3) An easy way to find a prime number is to loop from 2 to one less than the number
                   doing a modulus each time. If none of the modulus calculations reach 0 then the number is prime
                   
                   Console.WriteLine "x is a prime number" else "x is not a prime number"
            
                    
                */
            }
        }
    }
}
