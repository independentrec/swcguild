﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace calc_scratchpad
{
    class Calculator
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("What number would you like to factor?");
                string input = Console.ReadLine();

                int number;

                if (int.TryParse(input, out number))
                {
                    Console.WriteLine(number);
                    break;
                }
                else
                {
                    Console.WriteLine("That is not a number. Please enter a number to factor.");
                }
                /*
                    Start here
 
            A.  1) Solve math how to find all factors in number then create - if loop /i++ and 
                    Console.WriteLine "The factors of x are:"
                2) Solve math how to find if number is perfect then create - if loop and 
                    Console.WriteLine "x is a perfect number" else "x is not a perfect number"
                3) Solve math how to find if number is prime (precidence video) then create - if loop /i++ and 
                    Console.WriteLine "x is a perfect number" else "x is not a prime number"
            
                    
                */
            }
        }
    }
}

